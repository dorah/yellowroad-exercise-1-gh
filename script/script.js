//letrehoz egy a feladatban specifikalt tombot, es ezt visszaadja
function generateArray(count, max) {
    return new Array(count).fill(0).map((index, actual) => actual = getRandomNumber(0, max));
}

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//kap egy korabban legeneralt tombot, es kiszamolja, hogy legkevesebb hany lepessel lehet vegigmenni rajta a leirtak szerint
function calculateSteps(arr) {
    let latestValue = arr[0];
    let latestIndex = 0;

    let result = arr.reduce((result, current, index, array) => {
        if (latestIndex === index && index + latestValue < array.length) {
            latestIndex = getIndex(array, index, index + latestValue);
            latestValue = array[latestIndex];
            result.push(latestIndex);
        }

        return result;
    }, [0]);

    result.push('end');
    return result;
}

function getIndex(array, from, to) {
    let max = (array[from] - (to - from - from));
    let index = from;

    for (let i = from; i <= to; i++) {
        if (array[i] - (to - from - i) >= max) {
            max = array[i] - (to - from - i);
            index = i;
        }
    }

    return index;
}